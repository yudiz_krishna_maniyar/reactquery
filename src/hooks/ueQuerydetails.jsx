import { useQuery, useQueryClient } from "react-query";
import axios from "axios";

const featchquery = ({ queryKey }) => {
  const heroId = queryKey[1];
  return axios.get(`http://localhost:4000/Herors/${heroId}`);
};

export const useQuerydetails = (heroId) => {

  //intial query 
  const queryclient = useQueryClient();

  return useQuery(["super-hero", heroId], featchquery, {
    initialData: () => {
      const hero = queryclient
        .getQueryData("super-hero")
        ?.data?.find((hero) => hero.id === parseInt(heroId));
      if (hero) {
        return {
          data: hero,
        };
      } else {
        return undefined;
      }
    },
  });
};
