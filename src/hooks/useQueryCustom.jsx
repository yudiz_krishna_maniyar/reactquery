import { useQuery, useMutation , useQueryClient} from "react-query";
// import axios from "axios";
import {request} from '../utils/AxiosUtils'

const featchquery = () => {
  // return axios.get("http://localhost:4000/Herors");
  return request({url :'/Herors' })
};

const AddHero=(hero)=>{
  // return axios.post("http://localhost:4000/Herors", hero)
  return request({url: '/Herors', method : 'post', data: hero})
}


 export const UseQueryCustom = (onSuccess, onError) => {
  return useQuery("super-hero", featchquery, {
    // cacheTime:5000,
    // staleTime:0,
    // refetchOnMount: true,
    // refetchOnWindowFocus:'always'
    // refetchInterval:2000,
    // refetchIntervalInBackground:true
    //   enabled: false,
    onSuccess,
    onError,
    //   select: data => {
    //     const heros = data.data.map (hero => hero.name);
    //     return heros;
    //   },
  });
};

export const useAddHero =()=>{
  const queryclient = useQueryClient()
     return useMutation(AddHero, {
      // onSuccess:(data)=>{
      //   // queryclient.invalidateQueries('super-hero')
      //   queryclient.setQueryData('super-hero', (oldquerydata)=>{
      //        return {
      //         ...oldquerydata, 
      //         data : [...oldquerydata.data , data.data],
      //        }

             
      //   })
      // }

    onMutate:  async( newhero)=>{
     await queryclient.cancelQueries('super-hero')
     const previosdata = queryclient.getQueryData('super-hero')
     queryclient.setQueryData('super-hero', (oldquerydata)=>{
            return {
            ...oldquerydata, 
              data : [...oldquerydata.data ,{id : oldquerydata?.data?.length +1,...newhero} ,],
             }

             
        })
        return{
          previosdata,
        }
    },
    onerror:(_error , _hero, context)=>{
      queryclient.setQueryData('super-hero', context.previosdata)
    },
    onstalled:()=>{
      queryclient.invalidateQueries('super-hero')
    },
     })

}
