import { useQuery } from "react-query";
import axios from "axios";

const fetchquery = () => {
  return axios.get("http://localhost:4000/Herors");
};

const fetchfriend = () => {
  return axios.get("http://localhost:4000/friends");
};

export const RqParallel = () => {
  const {data: Heros}=useQuery('super-hero', fetchquery);
  const {data : friends}= useQuery('friends', fetchfriend);

  return <div>RqParallel</div>;
};
