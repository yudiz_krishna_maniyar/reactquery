import { useParams } from "react-router-dom";
import { useQuerydetails } from "../hooks/ueQuerydetails";

const RqHeroDetails = () => {
  const { heroId } = useParams();
  const { isLoading, data, error, isError } = useQuerydetails(heroId);
  if (isLoading) {
    return <h2>loading...</h2>;
  }
  if (isError) {
    return <h2>{error.message}</h2>;
  }
  return (
    <div>
  
      <h2>{data?.data.name} </h2>
    </div>
  );
};

export default RqHeroDetails;
