import React, { useEffect, useState } from "react";
import Axios from "axios";

const Heros = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error,setError]=useState(false);

  useEffect(() => {
    Axios.get("http://localhost:4000/Herors").then((res) => {
      setData(res.data);
      setLoading(false);
    }).catch((error)=>{
       setError(error.message);
       setLoading(false);
    })
  }, []);

  if (loading) {
    return <h2>Loading...</h2>;
  }
  if (error) {
    return <h2>{error}</h2>
    
  }
  return (
    <>
      {data.map((item) => {
        return <div key={item.name}>
        {item.name}
     <h2>{item.power}</h2>

        </div>
         
      })}
    </>
  );
};

export default Heros;
