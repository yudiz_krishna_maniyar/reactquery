import React,{useState} from "react";
import { Link } from "react-router-dom";
import {useAddHero, UseQueryCustom }from "../hooks/useQueryCustom";

const RqHeros = () => {

const [name, setName] = useState('');
const [alterEgo, setAlterEgo] = useState ('');


 const {mutate : AddHero}= useAddHero()

const handleAddHeroClick =()=>{
  console.log({name, alterEgo});
  const hero ={name, alterEgo}
  AddHero(hero)
}
  const onSuccess = (data) => {
    console.log(" query fetch before side effecr", data);
  };
  const onError = (error) => {
    console.log(" query fetch after side effecr", error);
  };

  const { isLoading, isError, isFetching, error, data , refetch} = UseQueryCustom(
    onSuccess,
    onError
  );

  if (isLoading || isFetching) {
    return <h2>loading...</h2>;
  }
  if (isError) {
    return <h2>{error.message}</h2>
  }

  return (
    <>
      <h1>Rq heros</h1>

      <div>
      
        
        <input type="text" value={alterEgo} onChange={e => setAlterEgo (e.target.value)} />
        <input type="text" value={name} onChange={e => setName (e.target.value)} />

<button onClick={handleAddHeroClick}>Add Hero</button>

      </div>
      <button onClick={refetch}>Refetch</button>
      {data?.data.map((hero)=>{
                return <div key={hero.id}>
                    <Link to={`/rqheros/${hero.id}`}>{hero.name}</Link>
                </div>
            })}
      {/* {data.map((heroname) => {
        return <div key={heroname}>{heroname}</div>;
      })} */}
    </>
  );
};

export default RqHeros;
