import { useQueries } from "react-query";
import axios from "axios";

const fetchquery = (heroId) => {
  return axios.get(`http://localhost:4000/Herors/${heroId}`);
};

export const RqDynamic = ({ heroids }) => {
  const QueryResult = useQueries(
    heroids.map(id => {
      return {
        queryKey: ['super-hero', id],
        queryFn: () => fetchquery(id),
      }
    })
  )
  console.log({QueryResult});
//   return <div>RqDynamic</div>;
};
