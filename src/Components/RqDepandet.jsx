import { useQuery } from "react-query";
import axios from "axios";

const featchqueryemail = (email) => {
  return axios.get(`http://localhost:3000/users/${email}`);
};

const featchquerydetails = (detailId) => {
  return axios.get(`http://localhost:3000/details/${detailId}`);
};

export const RqDepandet = ({ email }) => {
  const { data: user } = useQuery(["user", email], () =>
    featchqueryemail(email)
  );
  const detailId = user?.data.detailId;

  useQuery(["detail", detailId], () => featchquerydetails(detailId), {
    enabled: !!detailId,
  });
  return <div>RqDepandet</div>;
};
