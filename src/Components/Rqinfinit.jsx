import { useInfiniteQuery } from "react-query";
import axios from "axios";
import { Fragment } from "react";

const fetchquery = ({pageParam = 1}) => {
  return axios.get(`http://localhost:4000/color?_limit=2&_page=${pageParam}`);
};

export const Rqinfinit = () => {
  const { isLoading, isError, data, error , hasNextPage, fetchNextPage,isFetching, isFetchingNextPage} = useInfiniteQuery(["color"], fetchquery,
  {
    getNextPageParam:(_lastpage , pages)=>{
      if (pages.length < 4) {
        return pages.length +1
        
      }else{
        return undefined
      }
    }
  });

  if (isLoading) {
    return <h1>Loading</h1>;
  }

  if (isError) {
    return <h2>{error.message}</h2>;
  }

  return (
    <>
      <div>
        {data?.pages.map((group , i )=> {
          return (
            <Fragment key={i} > 
          
            {
                group.data.map((color)=>{
                   return <h2 key={color.id}> {color.id} {color.lable}</h2>
                })
            }
             </Fragment>
             )
        })}
      </div>
     <div><button disabled={!hasNextPage} onClick={fetchNextPage}>fetch</button></div> 
      <div>{isFetching && !isFetchingNextPage ? 'fetch... ' : null}
</div>
    </>
  );
};
