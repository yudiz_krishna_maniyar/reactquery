import './App.css';
import {Routes, Link, Route} from 'react-router-dom';
import RqHeros from './Components/RqHeros';
import Heros from './Components/Heros';
import HomePage from './Components/HomePage';
import {QueryClientProvider, QueryClient} from 'react-query';
import {ReactQueryDevtools} from 'react-query/devtools';
import RqHeroDetails from './Components/RqHeroDetails';
import { RqParallel } from './Components/RqParallel';
import { RqDynamic } from './Components/RqDynamic';
import { RqDepandet } from './Components/RqDepandet';
import { Rqpaginated } from './Components/RqPaginated';
import { Rqinfinit } from './Components/Rqinfinit';

function App () {
  const queryclient = new QueryClient()
  return (
    <QueryClientProvider client={queryclient}>
    <div className="App">
      
        <div>
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/heros">Traditional Super Heroes</Link>
              </li>
              <li>
                <Link to="/rqheros">RQ Super Heroes</Link>
              </li>
            </ul>
          </nav>
          <Routes>

      <Route path="/depandet" element={<RqDepandet email="km@gmail.com" />} />

            <Route path="/paginated" element={<Rqpaginated/>} />

            <Route path="/infinite" element={<Rqinfinit />} />



          <Route path="/dynamic" element={<RqDynamic heroids={[1, 3]}/>}  />

          <Route path="/parallel" element={<RqParallel />} />

           <Route path="/heros" element={<Heros />} />

            <Route path="/rqheros/:heroId" element={<RqHeroDetails />} />

            <Route path="/rqheros" element={<RqHeros />} />

           


            <Route path="/" element={<HomePage />} />

          </Routes>

        </div>

    <ReactQueryDevtools initialIsOpen={false} position='bottom-right'/>

    </div>
    </QueryClientProvider>
   
  );
}

export default App;
